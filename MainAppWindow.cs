﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CandidateTestSolution
{
    public partial class MainAppWindow : Form
    {
        struct XMLDataEntry
        {
            public string NodePath { get; set; }
            public uint Address { get; set; }

            public XMLDataEntry(string nodePath, uint address)
            {
                NodePath = nodePath;
                Address = address;
            }
        }

        private List<XMLDataEntry> XMLDataEntries;

        public MainAppWindow()
        {
            InitializeComponent();
        }

        private void LoadInputFile(string fileName)
        {
            InputFileContentsList.Items.Clear();

            if (!InputParser.OpenFile(fileName))
            {
                MessageBox.Show("Can't open input file!");
                return;
            }

            if (!TypeInfoParser.OpenFile("TypeInfos.json"))
            {
                MessageBox.Show("Can't open types file!");
                return;
            }

            uint currentOffset = 0;
            XMLDataEntries = new List<XMLDataEntry>();

            foreach (InputParser.InputFileFormat inputData in InputParser.FileContents)
            {
                TypeInfoParser.TypeInfo inputTypeInfo = TypeInfoParser.Types.FindTypeByName(inputData.Type);
                if (inputTypeInfo == null)
                    continue;

                foreach (KeyValuePair<string, string> inputPropertyTypeString in inputTypeInfo.Propertys)
                {
                    string itemPropertyString = string.Format("{0}.{1}", inputData.Tag, inputPropertyTypeString.Key);
                    uint propertyOffset = 0;

                    try
                    {
                        propertyOffset = inputTypeInfo.GetPropertyTypeSize(inputPropertyTypeString.Key);
                    }catch (KeyNotFoundException)
                    {
                        MessageBox.Show("Can't find size for type: " + inputPropertyTypeString.Value);
                        continue;
                    }

                    XMLDataEntry dataEntry = new XMLDataEntry(itemPropertyString, currentOffset);
                    XMLDataEntries.Add(dataEntry);

                    currentOffset += propertyOffset;

                    InputFileContentsList.Items.Add(itemPropertyString, true);
                }

                currentOffset = 0;
            }
        }

        private void SaveProcessedFile(string fileName)
        {
            StreamWriter streamWriter = new StreamWriter(fileName);
            uint linesWritten = 0;
            foreach (XMLDataEntry dataEntry in XMLDataEntries)
            {
                string outputFormatString = string.Format("<item Binding=\"Introduced\"><node-path>{0}</node-path><address>{1}</address></item>", dataEntry.NodePath, dataEntry.Address);
                streamWriter.WriteLine(outputFormatString);
                linesWritten++;
            }

            streamWriter.Close();

            MessageBox.Show("Done! Written " + linesWritten + " properties!");
        }

        private void OpenFileMenuItemClicked(object sender, EventArgs e)
        {
            OpenFileDialog dialog = (OpenFileDialog)FileDialogOpen;
            DialogResult dialogResult = dialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
                LoadInputFile(dialog.FileName);
        }

        private void SaveFileMenuItemClicked(object sender, EventArgs e)
        {
            SaveFileDialog dialog = (SaveFileDialog)FileDialogSave;
            DialogResult dialogResult = dialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
                SaveProcessedFile(dialog.FileName);
        }
    }
}