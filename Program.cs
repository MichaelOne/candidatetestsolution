﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CandidateTestSolution
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainAppWindow());
        }
    }

    /// <summary>
    /// The sole purpose of this class is to open and `parse` input file.
    /// </summary>
    static class InputParser
    {
        /// <summary>
        /// This describes the format of the input file.
        /// </summary>
        public struct InputFileFormat
        {
            public string Tag { get; set; }
            public string Type { get; set; }
            public string Address { get; set; }

            public InputFileFormat(string tagValue, string TypeValue, string AddressValue)
            {
                Tag = tagValue;
                Type = TypeValue;
                Address = AddressValue;
            }
        }

        public static List<InputFileFormat> FileContents;
        public static List<string> FileContentsHeader;

        //  Open a file and read it's data. Returns true if data read successfully, false if file can not be open.
        public static bool OpenFile(string FileName)
        {
            string currentLine;
            StreamReader fileStreamReader;

            //  Open file by it's name and return if file was not found.
            try
            {
                fileStreamReader = new StreamReader(FileName);
            }catch (FileNotFoundException)
            {
                return false;
            }

            FileContents = new List<InputFileFormat>();

            do
            {
                currentLine = fileStreamReader.ReadLine();
                if (currentLine == null)
                    continue;

                //  Check if read string is ill-formed.
                string Tag, Type, Address;
                int firstSemicolonPos = currentLine.IndexOf(';');
                if (firstSemicolonPos == -1)
                    return false;
                int secondSemicolonPos = currentLine.IndexOf(';', firstSemicolonPos + 1);

                Tag = currentLine.Substring(0, firstSemicolonPos);
                Type = currentLine.Substring(firstSemicolonPos + 1, secondSemicolonPos - firstSemicolonPos - 1);
                Address = currentLine.Substring(secondSemicolonPos + 1);

                //  If read values are not data, but the data names, then it's a header.
                if (Tag == "Tag" && Type == "Type" && Address == "Address")
                {
                    FileContentsHeader = new List<string>();
                    FileContentsHeader.Add(Tag);
                    FileContentsHeader.Add(Type);
                    FileContentsHeader.Add(Address);

                    continue;
                }

                //  If read values contain legitimate data, then store it.
                FileContents.Add(new InputFileFormat(Tag, Type, Address));
            } while (currentLine != null);

            fileStreamReader.Close();

            return true;
        }
    }

    /// <summary>
    /// This is responsible for storing types information, types are loaded from a JSON file.
    /// </summary>
    static class TypeInfoParser
    {
        static Dictionary<string, uint> TypeSizes = new Dictionary<string, uint>();

        public class TypeInfosData
        {
            public IList<TypeInfo> TypeInfos { get; set; }

            /// <summary>
            /// Use this to find what type corresponds to the given string.
            /// </summary>
            /// <param name="typeName">A plain string with type name. No quotes.</param>
            /// <returns>If successfull - returns a type reference, otherwise - null.</returns>
            public TypeInfo FindTypeByName(string typeName)
            {
                foreach (TypeInfo typeInfo in TypeInfos)
                    if (typeInfo.TypeName == typeName)
                        return typeInfo;

                return null;
            }
        }

        public class TypeInfo
        {
            public string TypeName { get; set; }
            public Dictionary<string, string> Propertys { get; set; }

            public uint GetPropertyTypeSize(string propertyName)
            {
                return TypeSizes[Propertys[propertyName]];
            }
        }

        public static TypeInfosData Types;

        /// <summary>
        /// Open file and parse it's contents as JSON.
        /// </summary>
        /// <param name="FileName">Path to a file that needs to be open.</param>
        /// <returns></returns>
        public static bool OpenFile(string FileName)
        {
            string fileContents = File.ReadAllText(FileName);
            if (fileContents == null)
                return false;

            TypeSizes["bool"] = 1;
            TypeSizes["int"] = 4;
            TypeSizes["double"] = 8;

            Types = JsonSerializer.Deserialize<TypeInfosData>(fileContents);

            return true;
        }
    }
}